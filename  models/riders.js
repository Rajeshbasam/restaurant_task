const mongoose = require('mongoose')
const schema = mongoose.Schema
const riderSchema = new schema({
    rider_name: {
        type: String,
        required: true
    },
    earnings: {
        type: Number,
        required: true
    },

})
const riderModel = mongoose.model('Customer', riderSchema, 'customers')

module.exports = riderModel
