const mongoose = require('mongoose')
const schema = mongoose.Schema
const customerSchema = new schema({
    customer_name: {
        type: String,
        required: true
    },
    product: {
        type: String,
        required: true
    },
    successful_orders: {
        type: Number,
        required: true
    },
    unsuccessful_orders: {
        type: Number,
        required: true
    },
    order_amount: {
        type: Number,
        required: true
    },
    orders: {
        type: Number,
        required: true
    }
})
const customerModel = mongoose.model('Customer', customerSchema, 'customers')

module.exports = customerModel
