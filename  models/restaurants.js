const mongoose = require('mongoose')
const schema = mongoose.Schema
const restaurantSchema = new schema({
    restaurant_name: {
        type: String,
        required: true
    },
    area: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },

})
const restaurantModel = mongoose.model('Restaurant', restaurantSchema, 'restaurants')

module.exports = restaurantModel
