const dotenv = require('dotenv')
dotenv.config()

const env = process.env.NODE_ENV

const dev = {
    app: {
        port: 3000
    },
    db: {
        mongo_connection_string: process.env.MONGO_CONNECTION_STRING
    },

}

const config = {
    dev

}
module.exports =
    config[env]