const customerModel = require('../ models/customers')
const restaurantModel = require('../ models/restaurants')
const riderModel = require('../ models/riders')




class Queries {


    //1. Get all the successful delivered orders placed by a customer which are above Rs 100

    async getCustomerSuccessfulOrders(successful_orders) {
        return await customerModel.aggregate([{
            $match: { successful_orders: { $gt: 100 } }
        }])
    }



    // Get all the trip history of Riders with total earnings.


    async totalEarningsOfRiders(earnings) {
        return await riderModel.aggregate([{
            $match: { totalSum: { $sum: "$earnings" } }
        }])
    }



    //Get list of all order of a restaurant with decreasing order total.

    async orderTotal(total) {
        return await restaurantModel.aggregate([{
            $match: {
                customers: {
                    order_amount: { $sum: -1 }
                }
            }
        }])
    }


    //Get all the food items that a customer has ordered in descending order of its total orders according to restaurant wise.

    async customerOrders(orders) {
        return await restaurantModel.aggregate([{
            $match: {
                customers: {
                    sort: { orders: -1 }
                }
            },
            $group: {
                restaurants: { $sum: 1 }
            }
        }])
    }
}

