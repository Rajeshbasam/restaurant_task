const faker = require('faker')
const restaurantServiceProvider = require('../serviceprovider/restaurantServiceProvider')
const mongoose = require('mongoose')
const config = require('../config/app')
const main = async () => {
    var restaurants = [];
    for (let i = 0; i < 100; i++) {
        let restaurant = {
            restaurant: faker.company.companyName(),
            area: faker.name.jobArea(),
            email: faker.internet.email(),
            phone: '+91' + faker.random.arrayElement([7, 8, 9]) + faker.random.number({ "min": 10000000, "max": 999999999 }),
        }
        restaurants.push(restaurant)
    }
    await restaurantServiceProvider.addRestaurants(restaurant)
}

(async function () {

    await mongoose.connect(config.db.mongo_connection_string, (err) => {
        if (err) {
            console.error('Error while connecting to mongoose', err)
        } else {
            console.info('Successfully connected mongoose')
            main()
        }
    })

})()