const faker = require('faker')
const restaurantServiceProvider = require('../serviceprovider/restaurantServiceProvider')
const customerDataServiceProvider = require('../serviceprovider/customerDataServiceProvider')
const mongoose = require('mongoose')
const config = require('../config/app')
const main = async () => {
    const restaurants = await restaurantServiceProvider.getRestaurants()
    var customers = [];
    for (let j = 0; j < restaurants.length; j++) {
        for (i = 0; i < 10; i++) {
            let customer = {
                customer_name: faker.name.findName(),
                product: faker.commerce.product(),
                successful_orders: faker.random.number(),
                unsuccessful_orders: faker.random.number(),
                order_amount: faker.random.number(),
                orders: faker.random.number()
            }
            customers.push(customer)
        }


    }
    await customerDataServiceProvider.addCustomers(customers)
}

(async function () {

    await mongoose.connect(config.db.mongo_connection_string, (err) => {
        if (err) {
            console.error('Error while connecting to mongoose', err)
        } else {
            console.info('Successfully connected mongoose')
            main()
        }
    })

})()