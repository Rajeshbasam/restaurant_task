const faker = require('faker')
const restaurantServiceProvider = require('../serviceprovider/restaurantServiceProvider')
const riderDataServiceProvider = require('../serviceprovider/riderDataServiceProvider')
const mongoose = require('mongoose')
const config = require('../config/app')
const main = async () => {
    const restaurants = await restaurantServiceProvider.getRestaurants()

    var riders = [];
    for (let j = 0; j < restaurants.length; j++) {
        for (i = 0; i < 15; i++) {
            let rider = {
                rider_name: faker.name.findName(),
                earnings: faker.random.number(),
            }
            riders.push(rider)
        }


    }
    await riderDataServiceProvider.addRiders(riders)
}

(async function () {

    await mongoose.connect(config.db.mongo_connection_string, (err) => {
        if (err) {
            console.error('Error while connecting to mongoose', err)
        } else {
            console.info('Successfully connected mongoose')
            main()
        }
    })

})()