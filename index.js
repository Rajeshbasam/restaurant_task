const express = require('express')
const app = express()
const bodyparser = require('body-parser')
app.use(bodyparser.json())
//const userrouter = require('./routes/user')
const mongoose = require('mongoose')
const config = require('./config/app')
mongoose.connect(config.db.mongo_connection_string, { useNewUrlParser: true, useUnifiedTopology: true })

mongoose.connection.on("error", err => {
    console.log('error occurred while connecting to database', err)
})
mongoose.connection.on('connected', (err, res) => {
    console.log('successfully connected to database')
})

//app.use('/', userrouter)

app.listen(config.app.port, (req, res) => {
    console.log('server up and listening on port 3000')
})