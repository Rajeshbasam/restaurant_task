const riderModel = require('../ models/riders')


class riderDataServiceProvider {
    async addRiders(riders) {
        return await riderModel.insertMany(riders)
    }
}

module.exports = new riderDataServiceProvider()