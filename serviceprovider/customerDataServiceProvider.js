const customerModel = require('../ models/customers')


class customerDataServiceProvider {
    async addCustomers(customers) {
        await customerModel.insertMany(customers)
    }
}

module.exports = new customerDataServiceProvider()