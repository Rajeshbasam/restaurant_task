const restaurantModel = require('../ models/restaurants')


class restaurantServiceProvider {
    async addRestaurants(restaurant) {
        return await restaurantModel.insertMany(restaurant)
    }
    async getRestaurants() {
        return await restaurantModel.find({})
    }
}

module.exports = new restaurantServiceProvider()